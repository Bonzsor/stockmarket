//
//  ViewController.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import UIKit

class StockListViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet var outerView: UIView!
    @IBOutlet weak var stockNavBar: UINavigationItem!
    @IBOutlet weak var stockTitleLabel: UILabel!
    @IBOutlet weak var stockSearchBar: UISearchBar!
    @IBOutlet weak var stockTable: UITableView!
    
    
    // UI Variables
    var fetchSpinner: UIView?
    
    
    // Variables
    var stockManager = StockManager()
    var stockList = [StockName]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Variables init
        stockList = stockManager.stockList
        stockManager.delegate = self
        
        // Navigation item init
        
        // Title label init
        stockTitleLabel.font = UIFont.boldSystemFont(ofSize: 30)
        stockTitleLabel.text = "Stocks you can buy"
        
        // Search bar init
        stockSearchBar.delegate = self
        stockSearchBar.placeholder = "Search"
        // this image removes the two lines from the searchbar borders
        stockSearchBar.backgroundImage = UIImage()
        
        // Table view init
        stockTable.delegate = self
        stockTable.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Reamoves the navigation bar when the view appear
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Render the navigation bar when the view disappear
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let stockDetailVC = segue.destination as! StockDetailViewController
        stockDetailVC.stockDetail = sender as? StockDetail
    }
}

extension StockListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        stockManager.searchStock(with: searchText)
    }
}

extension StockListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Create a header and add a title text
        return "STOCKS"
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        // Create  a footer and add a title text
        return " "
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Run when a table view cell is "pressed"
        
        // deselect the pressed row with animation
        tableView.deselectRow(at: indexPath, animated: true)
        
        // remove the keyboard and deactivate the searchbar
        stockSearchBar.resignFirstResponder()
        
        // show the loading spinner
        self.showSpinner(onView: self.outerView)
        
        // fetch the data
        stockManager.fetchDetails(with: stockList[indexPath.row])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Run when the tableview is scrolling
        
        // remove the keyboard and deactivate the searchbar
        stockSearchBar.resignFirstResponder()
    }
    
}

extension StockListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stockList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.stockListCellID, for: indexPath)
        cell.accessoryType = .disclosureIndicator
        
        // get the data from the class stockList
        let cellText = stockList[indexPath.row].symbol + " - " + stockList[indexPath.row].name
        cell.textLabel?.text = cellText
        
        return cell
    }
    
}

extension StockListViewController: StockManagerDelegate {
    
    func didShowErrorMessage(_ message: String) {
        // basic error shower view with a label
        self.stockSearchBar.resignFirstResponder()
        let errorView = UIView.init(frame: self.outerView.bounds)
        errorView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let text = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        text.text = message
        text.textColor = .black
        text.font = .systemFont(ofSize: 20)
        text.numberOfLines = 0
        text.textAlignment = .center
        text.center = errorView.center
        errorView.addSubview(text)
        self.outerView.addSubview(errorView)
        
        //NOTE: Egy helyen használom ezért nem írtam hozzá remove függvényt, mivel ha az API nem ad több adatot, akkor úgy is újra kell indítani az alkalmazásr.
    }
    
    func didUpdateList(_ list: [StockName]) {
        // repopulate the stock list
        self.stockList = list
        self.stockTable.reloadData()
    }
    
    
    func didSegue(_ stockDetail: StockDetail) {
        self.removeSpinner()
        performSegue(withIdentifier: K.stockDetailsSegueID, sender: stockDetail)
    }
    
}

extension StockListViewController {
    
    func showSpinner(onView: UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        fetchSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.fetchSpinner?.removeFromSuperview()
            self.fetchSpinner = nil
        }
    }
}
