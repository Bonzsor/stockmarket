//
//  StockDetailViewController.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import UIKit

class StockDetailViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var detailNavBar: UINavigationItem!
    @IBOutlet weak var detailTable: UITableView!
    
    // Variables
    var stockDetail: StockDetail? = nil
    var detailNames = [String]()
    var detailValues = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Variable init
        detailNames = stockDetail?.getNames() ?? detailNames
        detailValues = stockDetail?.getValues() ?? detailValues
        
        // Navigation item init
        detailNavBar.title = stockDetail?.symbol ?? "Try again"
        
        // Table view init
        detailTable.delegate = self
        detailTable.dataSource = self
        detailTable.register(UINib(nibName: K.detailCellNibName, bundle: nil), forCellReuseIdentifier: K.detailCellID)
    }
    
}

extension StockDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Create a header and add a title text
        return stockDetail?.name ?? "No corp. name"
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        // Create  a footer and add a title text
        return "Optional field for description of data above"
    }
    
}
extension StockDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.detailCellID, for: indexPath) as! StockDetailCell
        
        // custom cell values
        cell.detailName.text = detailNames[indexPath.row]
        cell.detailValue.text = detailValues[indexPath.row]
        
        return cell
    }
    
    
}
