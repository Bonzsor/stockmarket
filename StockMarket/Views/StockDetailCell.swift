//
//  StockDetailCell.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import UIKit

class StockDetailCell: UITableViewCell {
    
    // IBOutlets
    @IBOutlet weak var detailName: UILabel!
    @IBOutlet weak var detailValue: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // disable the cell = button usage
        self.selectionStyle = .none
        
        detailValue.textAlignment = .right
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
