//
//  StockSearchJsonData.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import Foundation

// Struct stack for JSON decoder

struct StockSearchJsonData: Codable {
    let bestMatches: [BestMatch]
}

struct BestMatch: Codable {
    let symbol, name: String

    enum CodingKeys: String, CodingKey {
        case symbol = "1. symbol"
        case name = "2. name"
    }
}
