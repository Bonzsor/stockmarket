//
//  StockManager.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import Foundation

protocol StockManagerDelegate {
    func didSegue(_ stockDetail: StockDetail)
    func didUpdateList(_ list: [StockName])
    func didShowErrorMessage(_ message: String)
}

class StockManager {
    
    var stockList = [StockName]()
    var delegate: StockManagerDelegate?
    
    init() {
        //NOTE: fejlesztésnél az apple stock appját tekintettem best practicenek, alapértelmezetten a kiemelt részvények látszanak, az összehez kereséssel lehet hozzáférni. Ezért itt előredefiniált adatokkal dolgozom.
        
        if K.Api.popularSymbols.count == K.Api.popularNames.count {
            for (index, symbol) in K.Api.popularSymbols.enumerated() {
                stockList.append(StockName(symbol: symbol, name: K.Api.popularNames[index], url: K.Api.getDetailUrl(from: symbol)))
            }
        }
    }
    
    func fetchDetails(with stockName: StockName) {
        // Fetching the details with an url
        
        guard let url = URL(string: stockName.url) else { return }
        let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                // create an url session in a new thread
        
                if let error = error {
                    print(error)
                    return
                }
                
                if let data = data {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            let details = self.parseDetailJson(with: json, symbol: stockName.symbol, name: stockName.name)
                            
                            DispatchQueue.main.async {
                                // delegate in the main thread
                                self.delegate?.didSegue(details)
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        return
                    }
                }
        })
        task.resume()
    }
        
    private func parseDetailJson(with json: [String: Any], symbol: String, name: String) -> StockDetail {
        
        // default value for error
        let errorDetail = StockDetail(name: name, symbol: symbol, open: "No information", high: "No information", low: "No information", close: "No information", volume: "No information")
        
        //NOTE: Ha az API elérte a percenkénti response limitjét akkor is az error érték jelenik meg.
        
        // decode the JSON mnaually

        guard let metaData = json["Meta Data"] as? [String: Any] else {
            print("Meta Data JSON parse error")
            return errorDetail
        }
        guard let lastRefreshed = metaData["3. Last Refreshed"] as? String else {
            print("Last Refreshed JSON parse error")
            return errorDetail
        }
        guard let timeSeries = json["Time Series (Daily)"] as? [String: Any] else {
            print("Time Series JSON parse error")
            return errorDetail
        }
        guard let details = timeSeries[lastRefreshed] as? [String: Any] else {
            print("Details JSON parse error")
            return errorDetail
        }
        guard let open = details["1. open"] as? String else {
            print("Open detail JSON parse error")
            return errorDetail
        }
        guard let high = details["2. high"] as? String else {
            print("High detail JSON parse error")
            return errorDetail
        }
        guard let low = details["3. low"] as? String else {
            print("Low detail JSON parse error")
            return errorDetail
        }
        guard let close = details["4. close"] as? String else {
            print("Close detail JSON parse error")
            return errorDetail
        }
        guard let volume = details["5. volume"] as? String else {
            print("Volume detail JSON parse error")
            return errorDetail
        }
        
        return StockDetail(name: name, symbol: symbol, open: open, high: high, low: low, close: close, volume: volume)
        
    }
    
    
    func searchStock(with text: String) {
        // Fetch the data from the API with a search text
        
        if text != "" {
            guard let url = URL(string: K.Api.getSearchUrl(text: text)) else { return }
                let request = URLRequest(url: url)
                let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                    // create an url session in a new thread
            
                    if let error = error {
                        print(error)
                        return
                    }
            
                    if let data = data {
                        // decode with the StockSearchJsonData.swift
                        if let decodedResponse = try? JSONDecoder().decode(StockSearchJsonData.self, from: data) {
                            
                            var sList = [StockName]()
                            // itarate to populate the new list
                            for row in decodedResponse.bestMatches {
                                sList.append(StockName(symbol: row.symbol, name: row.name, url: K.Api.getDetailUrl(from: row.symbol)))
                            }
                            
                            DispatchQueue.main.async {
                                // delegate in the main thread
                                self.delegate?.didUpdateList(sList)
                            }
                            return
                        } else {
                            DispatchQueue.main.async {
                                // delegate in the main thread
                                self.delegate?.didShowErrorMessage("Error: Too many API request")
                                
                                //NOTE: limitált az ingyenes API response mennyisége így itt meghívok egy delegatet
                            }
                            
                        }
                    }
                    DispatchQueue.main.async {
                        // delegate in the main thread
                        self.delegate?.didUpdateList([StockName]())
                    }
               })
               task.resume()
            
        } else {
            // if the searchtext is empty, then we load the popular list
            self.delegate?.didUpdateList(stockList)
        }
    }
        
    
}

    
