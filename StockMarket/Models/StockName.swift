//
//  StockName.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import Foundation

struct StockName {
    // Data model for the stock names with symbol and daily detail API URL
    
    let symbol: String
    let name: String
    let url: String
}
