//
//  StockDetail.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import Foundation

struct StockDetail {
    // Data model for the daily details
    
    let name: String
    let symbol: String
    let open: String
    let high: String
    let low: String
    let close: String
    let volume: String
    
    func getNames() -> [String] {
        return ["Open", "High", "Low", "Close", "Volume"]
    }
    
    func getValues() -> [String] {
        return [open, high, low, close, volume]
    }
}
