//
//  Constraints.swift
//  StockMarket
//
//  Created by Szabó Zsombor on 2020. 07. 10..
//  Copyright © 2020. Szabo Zsombor. All rights reserved.
//

import Foundation

struct K {
    // Struct for the project static constants and functions
    
    static let stockListCellID = "StockCell"
    static let stockDetailsSegueID = "StockDetailsSegue"
    
    static let detailCellID = "DetailCell"
    static let detailCellNibName = "StockDetailCell"
    
    struct Api {
        static let baseUrl = "https://www.alphavantage.co/query?"
        static let function = "function=TIME_SERIES_DAILY"
        static let symbolTag = "&symbol="
        static let apiKeyTag = "&apikey="
        
        static let apiKeyValue = "YX3FIZCYBKAEVQ9Z"
        static let popularSymbols = ["IBM" ,"AAPL", "BA", "DIS", "GE", "HD"]
        static let popularNames = ["International Business Machines Corp.", "Apple Inc.", "The Boeing Company", "The Walt Disney Company", "General Electric Company", "The Home Depot Inc."]
        
        static func getDetailUrl(from symbol: String) -> String {
            return baseUrl + function + symbolTag + symbol + apiKeyTag + apiKeyValue
        }
        
        static func getSearchUrl(text: String) -> String {
                return "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=\(text)&apikey=\(apiKeyValue)"
        }
    }
}
